import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
        name="motivationbuddy",
        version="0.1",
        description="motivation buddy checks the progress of your repo and sends you motivation mails.",
        long_description=long_description,
        long_description_content_type="text/markdown",
        url="",
        packages=setuptools.find_packages(),
        packages_dir={'': "motivationbuddy"},
        package_data={'': ['templates/*']},
        author="Timo Stüber",
        author_email="omit66@gmail.com",
        install_requires=["requests", "Click", "mako"],
        classifiers=[
            "Programming Language :: Python 3",
        ],
        python_requires=">3.6",
        entry_points={
            'console_scripts': ['motivationbuddy=motivationbuddy.cli:main'],
        }
        )
