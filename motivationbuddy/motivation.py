# -*- coding: utf-8 -*-

import io
import json
import random
import os


import requests

from . import git_metrics
from . import html_builder


def random_gif(tag):
    api = "https://api.tenor.co/v1/search?api_key=LIVDSRZULELA&tag={}".format(
            tag)
    resp = requests.get(api)
    results = json.loads(resp.text)['results']
    entry = random.choice(results)
    return entry['media'][0]['gif']['url']


def send_mail(sender, s_name, receiver, r_name, htmlpart, textpart=None):
    print("sending email to {}...".format(receiver))

    resp = requests.post(
            "https://api.mailjet.com/v3.1/send",
            auth=("23d8c47c21c7601a076c230746ef1a75",
                  "e966ee7e3b0509010315d942f57a6412"),
            json={
                'Messages': [
                    {
                     "From": {
                            "Email": sender,
                            "Name": s_name
                     },
                     "To": [
                            {
                                    "Email": receiver,
                                    "Name": r_name
                            }
                     ],
                     "Subject": "Motivation Message",
                     # "TextPart": report,
                     "HTMLPart": htmlpart
                     }
                ]
            })
    if resp.status_code != 200:
        print(resp.content)


def get_tag(progress):
    if not progress:
        return random.choice(["vicarious-embarrassment",
                              "shame",
                              "never-gonna-give-you-up",
                              "ridiculous",
                              "disappointed",
                              "mad"])
    return random.choice(["good-job",
                          "hero",
                          "like-a-boss",
                          "good-boy"])


def create_gitlabci_file():
    import pkg_resources

    template = pkg_resources.resource_string(".".join((__package__, "templates")),
                                             'gitlab-ci.yml.template')
    write_file(".gitlab-ci.yml", template)


def write_file(name, content):
    if isinstance(content, str):
        content = content.encode("utf-8")
    with io.open(name, "wb") as f:
        f.write(content)


def init():
    create_gitlabci_file()


def check_progress():
    """Calc a score using the git metrics."""
    progress = 0.0
    m = git_metrics.GitMetrics()
    metrics = m.get_metrics()
    # ca. 3000 per page
    added_lines = metrics['insertions'] - metrics['deletions']
    progress += added_lines / 300
    progress += metrics['added_images'] * 0.5
    progress += metrics['added_files'] * 0.2
    return progress >= 2.0


def run_check(email):
    # now it begins...
    progress = check_progress()
    gif = random_gif(get_tag(progress))
    # print(gif)
    m = git_metrics.GitMetrics()

    title = "Your Daily Portion of Motivation"
    print("REPORT:")
    m.print_metrics()
    report = m.get_metrics()
    if progress:
        # I have no idea why this is not working
        msg = r'You are such a good boy🐶!!'
    else:
        msg = 'I\'m not mad, I\'m just disappointed!!'

    htmlbuilder = html_builder.HtmlBuilder(title, msg, progress, report, gif)

    # write the gitlab page
    artifact_dir = "public"
    if not os.path.exists(artifact_dir):
        os.mkdir(artifact_dir)
    write_file(os.path.join("public", "index.html"), htmlbuilder.create_page())

    # send an email
    if email:
        a_mail, a_name = m.get_author()
        # dont spam using emails... this could be used for the scheduler
        send_mail("motivationbuddy@web.de", "motivationbuddy", a_mail,
                  a_name, htmlbuilder.create_email())


if __name__ == "__main__":
    # send_simple_message()
    # print(random_gif("shame"))
    pass
