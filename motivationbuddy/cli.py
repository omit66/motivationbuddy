import click
from . import motivation


@click.group()
def main():
    pass


@click.command()
def init():
    motivation.init()


@click.command()
@click.option('--email', is_flag=True)
def run_check(email):
    motivation.run_check(email)


main.add_command(init)
main.add_command(run_check)


if __name__ == "__main__":
    main()
