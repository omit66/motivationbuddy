"""
Extract some progress infos using the git.
"""
import io
import os
import subprocess


class GitMetrics:

    def __init__(self):
        self.commit_hash = self.get_commit_before_today()

    def get_author(self):
        cmd = "git show -s --format=%ae;%an {}".format(self.commit_hash)
        process = subprocess.Popen(cmd.split(), stdout=subprocess.PIPE,
                                   universal_newlines=True)
        out, err = process.communicate()
        # print(out, err)
        mail, name = out.replace("\n", "").split(";")
        return mail, name

    def get_commit_before_today(self):
        print("get commit before today")
        cmd = 'git log --until=today.0:00am -1 --pretty=format:%h'
        process = subprocess.Popen(cmd.split(), stdout=subprocess.PIPE,
                                   universal_newlines=True)
        out, err = process.communicate()
        print(out, err)
        self.commit_hash = out
        return out

    def get_shortstat(self):
        print("get shortstat: files changed, insertions, deletions")
        cmd = 'git diff {} --shortstat'.format(self.commit_hash)
        process = subprocess.Popen(cmd.split(), stdout=subprocess.PIPE,
                                   universal_newlines=True)
        out, err = process.communicate()
        # print(out, err)
        result = {}
        if out.strip() == "":
            print("No diff found...")
            result["files_changed"] = 0
            result["insertions"] = 0
            result["deletions"] = 0
            return result
        shortstat = [ele.split()[0] for ele in out.split(",")]
        result["files_changed"] = int(shortstat[0])
        result["insertions"] = int(shortstat[1])
        result["deletions"] = int(shortstat[2])
        # print(result)
        return result

    def get_files_by_status(self, status):
        # print("get files by status")

        cache = getattr(self, "status_" + status, None)
        if cache is not None:
            # print("cached status found...")
            return cache
        cmd = 'git diff --name-only {} --diff-filter={}'.format(
                self.commit_hash, status)
        process = subprocess.Popen(cmd.split(), stdout=subprocess.PIPE,
                                   universal_newlines=True)
        out, err = process.communicate()
        # print(out, err)
        result = [e for e in out.split("\n") if e != ""]
        setattr(self, "status_" + status, result)
        return result

    def get_num_added_images(self):
        files = self.get_files_by_status("A")
        num = 0
        for f in files:
            if os.path.splitext(f)[1].lower() in ['.jpg', '.jpeg', 'png']:
                num += 1
        return num

    def get_num_added_files(self):
        return len(self.get_files_by_status("A"))

    def get_num_renamed_files(self):
        return len(self.get_files_by_status("R"))

    def get_num_deleted_files(self):
        return len(self.get_files_by_status("D"))

    def print_metrics(self):
        print(self.report())

    def report(self):
        output = io.StringIO()
        output.write("<h4> Stats </h4>")
        for k, v in self.get_metrics().items():
            output.write("  {}: {}\n".format(k, v))
        return output.getvalue()

    def get_metrics(self):
        result = {}
        shortstat = self.get_shortstat()
        result.update(shortstat)
        result["added_files"] = self.get_num_added_files()
        result["added_images"] = self.get_num_added_images()
        result["deleted_files"] = self.get_num_deleted_files()
        result["renamed_files"] = self.get_num_renamed_files()

        return result


if __name__ == "__main__":
    m = GitMetrics()
    m.print_metrics()
