<html>
    <h3>${title}</h3>
    <span
    % if progress:
        style="color:#008503;"
    % else:
        style="color:#d10000;"
    %endif
    >${msg}</span><br>
    <ul style="Margin:0; padding:0;">
    % for k, v in report.items():
        <li style="Margin:0 0 1em; list-style:disc inside; mso-special-format:bullet;">${k}: ${v}</li>
    %endfor
    </ul>
    <img src="${gif}">
</html>
