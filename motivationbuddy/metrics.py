"""
Create Metrics of the latex folder structure.
"""
import os


def run_handlers(start, handlers, file_ext="tex"):
    """Open all files and run hanlders... Merge the result."""
    res = {}
    for thing in os.listdir(start):
        thing = os.path.join(start, thing)
        if os.path.isfile(thing):
            if thing.endswith('.' + file_ext):
                with open(thing, 'r') as f:
                    for h in handlers:
                        f.seek(0)
                        if h.__name__ in res:
                            res[h.__name__] = res[h.__name__] + h(f)
                        else:
                            res[h.__name__] = h(f)

    for thing in os.listdir(start):
        thing = os.path.join(start, thing)
        if os.path.isdir(thing):
            recursion = run_handlers(thing, handlers)
            for key, item in recursion.items():
                if key in res:
                    res[key] = res[key] + item
                else:
                    res[key] = item
    return res


def count_lines(f):
    return len(f.readlines())


def count_chars(f):
    return len(f.read())


def count_figs(f):
    pass


def count_tables(f):
    pass


def count_section(f):
    pass



def pdf_pages(f):
    import PyPDF2
    reader = PyPDF2.PdfFileReader(f)
    return reader.getNumPages()


def calculate_metrics(path):
    if not os.path.exists(path):
        raise ValueError("Path not found!")
    tex_handlers = []
    tex_handlers.append(count_lines)
    tex_handlers.append(count_chars)
    print("metrics: ", run_handlers(path, tex_handlers))
    pdf_handlers = [pdf_pages]
    print("metrics: ", run_handlers(path, pdf_handlers, file_ext="pdf"))


# number of lines in pdf
# anzahl bilder
# anzahl kapitel
# tables


if __name__ == "__main__":

    calculate_metrics("code")
