"""Builder for the html report files"""

import pkg_resources


from mako.template import Template


class HtmlBuilder():

    def __init__(self, title, msg, progress, report, gif):
        self.title = title
        self.msg = msg
        self.progress = progress
        self.report = report
        self.gif = gif

    def render_template(self, template_name):
        content = self._get_template(template_name)
        template = Template(content)

        return template.render(title=self.title,
                               msg=self.msg,
                               progress=self.progress,
                               report=self.report,
                               gif=self.gif)

    def create_email(self):
        return self.render_template("email.txt")

    def create_page(self):
        return self.render_template("page.txt")

    def _get_template(self, name):
        template_folder = '.'.join((__package__, "templates"))
        template = pkg_resources.resource_string(template_folder, name)
        return template
