# motivationbuddy
This is a fun project which I have done for a friend of mine.

## How it works
The motivationbuddy uses git to extract some infos.
A progress and motivation report is generated.

##  Installation
You have to install the motivationbuddy and create a gitlab-ci to make things work.

### local
Install the python package from git:
``
pip install git+https://gitlab.com/omit66/motivationbuddy
``

Use motivationbuddy to create a gitlab-ci file. 

 
``
motivationbuddy init
``

### gitlab

#### pages 
After each commit a gitlab page is created showing the progress report.
Go to settings/pages to see the url.

#### scheduler
Additionally, configure a scheduler to run nightly jobs and send you motivation emails.



Have fun 🙃 🎉